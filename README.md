# flickr-coroutines

This solution doesn't boast anything magnificent on the frontend, just a responsive list of Flickr Public Feed images,
however underneath the app is using some of the latest architecture components and approaches in Android development.

I have utilised Kotlin Coroutines for background work while keeping them out of the ViewModel (they reside entirely in the
repository). The parallel benefit of Kotlin Coroutines are utilised when inserting fetched data into the local store, a
Room database.

ContentStatus is a neat little sealed class that helps track the status of a content load and convey
results to observers along the way. It's use in conjunction with ConnectivityManager allows the app to respond flexibly to
connectivity issues and paves the way for versatile content to be added in the future, should that be needed.

Retrofit and Moshi have been utilised for accessing and parsing the Flickr Public Feed API, with a custom interceptor,
JsonCleaningInterceptor, setup to remove JSONP-ready wrapping of the endpoint response prior to parsing by Moshi (done quite
crudely, with a whole string extraction).

Koin has been used for Dependency Injection, assisting greatly towards the SOLID architecture. Some foundations for unit and instrumentation
testing have been setup, I was particularly focusing on tests surrounding coroutines.

# Disclaimer

Intentionally targeting Android SDK level 28 and nothing prior to minimise testing scope while maximising exposure to recent
APIs and platform limitations.

# Next steps would be:
* License attribution.
* a Swipe-to-refresh mechanism so user can kee getting new pictures, at the moment the app will hold onto one set and show it
"forever".
* a Progress Bar to allude to network commmunications.
* throttling for network requests.
* retry button for the case where no content exists and fetching failed.
* more tests.
* adjust landscape image height according to screen dimensions.

