package au.com.coreform.android.flickrcoroutines.data

import android.net.Uri
import androidx.databinding.BaseObservable
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.ZonedDateTime

@Entity(
    tableName = "local_media"
)
class LocalMediaItem: BaseObservable() {
    companion object {
        @JvmStatic
        fun from(aPublicFeedItem: PublicFeedItem): LocalMediaItem {
            val newLocalMediaItem = LocalMediaItem()
            newLocalMediaItem.title = aPublicFeedItem.title
            newLocalMediaItem.uri = Uri.parse(aPublicFeedItem.media?.mediaUrl)
            newLocalMediaItem.taken_datetime = ZonedDateTime.parse(aPublicFeedItem.date_taken)
            newLocalMediaItem.taken_timestamp = newLocalMediaItem.taken_datetime?.toEpochSecond()
            return newLocalMediaItem
        }

        @JvmStatic
        fun from(publicFeedList: List<PublicFeedItem>): List<LocalMediaItem> {
            return publicFeedList.map { it ->
                LocalMediaItem.from(it)
            }
        }
    }
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null

    var title: String? = null

    var uri: Uri? = null

    var taken_timestamp: Long? = null

    var taken_datetime: ZonedDateTime? = null
}