package au.com.coreform.android.flickrcoroutines.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FlickrMedia(
    @Json(name = "m") val mediaUrl: String
)