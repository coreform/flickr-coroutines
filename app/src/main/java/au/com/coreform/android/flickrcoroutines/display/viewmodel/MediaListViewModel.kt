package au.com.coreform.android.flickrcoroutines.display.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import au.com.coreform.android.flickrcoroutines.data.ContentStatus
import au.com.coreform.android.flickrcoroutines.data.LocalMediaItem
import au.com.coreform.android.flickrcoroutines.repositories.LocalMediaRepository

class MediaListViewModel(application: Application,
                         val mediaRepo: LocalMediaRepository): AndroidViewModel(application)  {

    var mediaContentLDP: Pair<LiveData<ContentStatus<List<LocalMediaItem>>>, LiveData<List<LocalMediaItem>>>
    var mediaContentStatus: LiveData<ContentStatus<List<LocalMediaItem>>>
    var mediaList: LiveData<List<LocalMediaItem>>

    var isSeeking = true
    var isInError = false

    init {
        mediaContentLDP = mediaRepo.ldLocalMedia()
        mediaContentStatus = mediaContentLDP.first
        mediaList = mediaContentLDP.second
    }
}