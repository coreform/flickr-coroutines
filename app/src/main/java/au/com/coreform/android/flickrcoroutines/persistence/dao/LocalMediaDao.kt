package au.com.coreform.android.flickrcoroutines.persistence.dao

import androidx.room.*
import au.com.coreform.android.flickrcoroutines.data.LocalMediaItem

@Dao
interface LocalMediaDao {
    @Transaction
    open suspend fun cleanslateInsertAllMedia(localMediaList: List<LocalMediaItem>) {
        deleteAll()
        insertAllMedia(localMediaList)
    }

    @Query("SELECT * FROM local_media ORDER BY taken_timestamp DESC")
    suspend fun listLocalMedia(): List<LocalMediaItem>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMedia(aLocalMediaItem: LocalMediaItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllMedia(localMediaList: List<LocalMediaItem>)

    @Query("DELETE FROM local_media")
    suspend fun deleteAll()
}