package au.com.coreform.android.flickrcoroutines.data

import com.squareup.moshi.JsonClass

/*
 * Represents the top-evel JSON Object in the Flickr Public Feed endpoint.
 */
@JsonClass(generateAdapter = true)
data class PublicFeedHeader(val title: String, val items: List<PublicFeedItem>)