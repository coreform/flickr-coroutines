package au.com.coreform.android.flickrcoroutines.comms

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody

/*
 * Removes the javascrit function wrapping a JSONP-ready response from Flickr Public Feed endpoint.
 */
class JsonCleaningInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val response = chain.proceed(request)
        val rawJson = response.body()?.string()

        val cleansedJson = cleanseRawJson(rawJson)

        return response.newBuilder()
            .body(ResponseBody.create(response.body()?.contentType(), cleansedJson)).build()
    }

    fun cleanseRawJson(rawJson: String?): String? {
        Log.d("FlickrPublicFeedServiceAPI", "rawJson: ${rawJson?.length}")
        val cleansedJson  = rawJson
            ?.removePrefix("jsonFlickrFeed(")
            ?.removeSuffix(")")
        Log.d("FlickrPublicFeedServiceAPI", "cleansedJson: ${cleansedJson?.length}")
        return cleansedJson
    }
}