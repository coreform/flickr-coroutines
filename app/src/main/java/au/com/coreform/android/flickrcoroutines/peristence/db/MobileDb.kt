package au.com.coreform.android.flickrcoroutines.peristence.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import au.com.coreform.android.flickrcoroutines.data.LocalMediaItem
import au.com.coreform.android.flickrcoroutines.persistence.dao.LocalMediaDao

@Database(
    entities = arrayOf(LocalMediaItem::class),
    version = 1,
    exportSchema = true
)
@TypeConverters(RoomTypeConverters::class)
abstract class MobileDb: RoomDatabase() {
    abstract fun flickrMediaDao(): LocalMediaDao
}