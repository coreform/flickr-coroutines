package au.com.coreform.android.flickrcoroutines.repositories

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo
import android.util.Log
import androidx.annotation.UiThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.MutableLiveData
import au.com.coreform.android.flickrcoroutines.comms.FlickrPublicFeedService
import au.com.coreform.android.flickrcoroutines.data.ContentStatus
import au.com.coreform.android.flickrcoroutines.data.LocalMediaItem
import au.com.coreform.android.flickrcoroutines.data.PublicFeedHeader
import au.com.coreform.android.flickrcoroutines.persistence.dao.LocalMediaDao
import kotlinx.coroutines.*
import java.sql.SQLException

class LocalMediaRepository(private val localMediaDao: LocalMediaDao,
                           private val flickrService: FlickrPublicFeedService,
                           private val connectivityManager: ConnectivityManager
) {
    private val coroutineScope = CoroutineScope(Dispatchers.IO + Job())

    private lateinit var obsvContentStatus: MutableLiveData<ContentStatus<List<LocalMediaItem>>>
    private lateinit var obsvLocalMediaList: MutableLiveData<List<LocalMediaItem>>

    private var currentNetworkCallback: ConnectivityManager.NetworkCallback? = null
    private val defaultNetworkCallback: ConnectivityManager.NetworkCallback =
        object: ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network?) {
            super.onAvailable(network)
            Log.d("LocalMediaRepository", "Connectivity reestablished, retrying...")
            val pairObsv = ldLocalMedia()
            obsvContentStatus = pairObsv.first
            obsvLocalMediaList = pairObsv.second
        }
    }

    fun isConnectionReady(): Boolean {
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnected == true
        return isConnected
    }

    @UiThread
    fun ldLocalMedia(): Pair<MutableLiveData<ContentStatus<List<LocalMediaItem>>>,
            MutableLiveData<List<LocalMediaItem>>> {
        Log.d("LocalMediaRepository", "ldLocalMedia...")
        currentNetworkCallback?.let {
            connectivityManager.unregisterNetworkCallback(it)
        }
        currentNetworkCallback = null
        if (!::obsvContentStatus.isInitialized) {
            obsvContentStatus = MutableLiveData()
            obsvContentStatus.value = ContentStatus.Seeking(::ldLocalMedia)
        }
        if (!::obsvLocalMediaList.isInitialized) {
            obsvLocalMediaList = MutableLiveData()
        }
        // Check connectivity, return early if no connection
        if (!isConnectionReady()) {
            obsvContentStatus.value = ContentStatus.FetchFailed(::ldLocalMedia,
                Exception("No connection to the Internet."))
            currentNetworkCallback = defaultNetworkCallback
            connectivityManager.registerDefaultNetworkCallback(currentNetworkCallback)
            return Pair(obsvContentStatus, obsvLocalMediaList)
        }

        coroutineScope.launch {
            withContext(Dispatchers.Main) {
                obsvContentStatus.value = ContentStatus.Retrieving(::ldLocalMedia)
                try {
                    Log.d("LocalMediaRepository", "Retrieving content...")
                    val mediaFromDb = runBlocking(Dispatchers.IO) {
                        async { getLocalMediaFromDb() }.await()
                    }
                    Log.d("LocalMediaRepository", "retrieved local media: ${mediaFromDb.size}")
                    // TODO check if local data is stale or expired
                        //obsvContentStatus.value = ContentStatus.LocalStaleFetching(::ldLocalMedia, mediaFromDb)
                    // If there is no data in DB, then definitely want to try and fetch some
                    // Note: could go further here: retries with exponential backoff...time permitting...
                    // TODO request throttling: could check data against some stored last successful response
                    // timestamp...time permitting...
                    if (mediaFromDb.isEmpty()) {
                        Log.d("LocalMediaRepository", "no local media, so fetching...")
                        obsvContentStatus.value = ContentStatus.NothingLocalFetching(::ldLocalMedia)
                        val mediaFromFlickr = runBlocking(Dispatchers.IO) {
                            async { fetchFlickrPublicFeedMedia() }.await()
                        }
                        Log.d("LocalMediaRepository", "fetched media: ${mediaFromFlickr.size}")
                        obsvContentStatus.value = ContentStatus.SatisfiedExternally(::ldLocalMedia, mediaFromFlickr)
                        obsvLocalMediaList.value = mediaFromFlickr
                        Log.d("LocalMediaRepository", "inserting fetched media into local storage...")
                        async(Dispatchers.IO) {
                            return@async insertFlickrMediaToDb(mediaFromFlickr)
                        }
                    } else {
                        obsvContentStatus.value = ContentStatus.SatisfiedLocally(::ldLocalMedia, mediaFromDb)
                        obsvLocalMediaList.value = mediaFromDb
                    }
                } catch (e: SQLException) {
                    obsvContentStatus.value = ContentStatus.FetchFailed(::ldLocalMedia, e)
                }
            }
        }
        return Pair(obsvContentStatus, obsvLocalMediaList)
    }

    @WorkerThread
    suspend fun getLocalMediaFromDb(): List<LocalMediaItem> {
        return localMediaDao.listLocalMedia()
    }

    @WorkerThread
    suspend fun insertFlickrMediaToDb(mediaFromFlickr: List<LocalMediaItem>) {
        return localMediaDao.cleanslateInsertAllMedia(mediaFromFlickr)
    }

    @UiThread
    suspend fun fetchFlickrPublicFeedMedia(): List<LocalMediaItem> {
        var data: PublicFeedHeader? = null
        try {
            Log.d("LocalMediaRepository", "Fetching content...")
            val response = runBlocking(Dispatchers.IO) {
                async { flickrService.fetchContent() }.await()
            }
            if (response.isSuccessful) {
                data  = response.body()
                Log.d("LocalMediaRepository",
                    "Comms success! ${data?.title} , size: ${data?.items?.size}")
                Log.d("LocalMediaRepository",
                    "items: ${data?.items.toString()}")
            } else {
                Log.d("LocalMediaRepository", "Comms error: ${response.code()}")
            }
        } catch (e: Exception) {
            Log.d("LocalMediaRepository", "Exception ${e.message}")
        }
        // Map FlickrPublicFeed to local domain
        data?.let { return LocalMediaItem.from(it.items) }
        return emptyList()
    }
}