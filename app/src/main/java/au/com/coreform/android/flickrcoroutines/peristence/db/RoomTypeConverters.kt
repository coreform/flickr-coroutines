package au.com.coreform.android.flickrcoroutines.peristence.db

import android.net.Uri
import androidx.room.TypeConverter
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class RoomTypeConverters {
    companion object {
        private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

        @TypeConverter
        @JvmStatic
        fun toZonedDateTime(value: String?):ZonedDateTime? {
            return value?.let {
                return formatter.parse(value, ZonedDateTime::from)
            }
        }

        @TypeConverter
        @JvmStatic
        fun fromZonedDateTime(date: ZonedDateTime?): String? {
            return date?.format(formatter)
        }

        @TypeConverter
        @JvmStatic
        fun toUri(value: String?): Uri? {
            return Uri.parse(value)
        }

        @TypeConverter
        @JvmStatic
        fun fromUri(hypermedia: Uri?): String? {
            return hypermedia.toString()
        }
    }
}