package au.com.coreform.android.flickrcoroutines.display.viewmodel

import android.net.Uri
import android.view.View
import androidx.databinding.BindingAdapter
import com.facebook.drawee.view.SimpleDraweeView

class BindingAdapters {
    companion object {
        @BindingAdapter("visibleGone")
        @JvmStatic
        fun showHide(view: View, shouldShow: Boolean ) {
            view.setVisibility(if (shouldShow) View.VISIBLE else View.GONE)
        }

        @BindingAdapter("imageUrl")
        @JvmStatic
        fun displayExternalImage(view: SimpleDraweeView, imageUri: Uri) {
            view.setImageURI(imageUri.toString())
        }
    }
}