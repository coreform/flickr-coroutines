package au.com.coreform.android.flickrcoroutines

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MobileApp: Application() {

    override fun onCreate() {
        super.onCreate()

        try {
            Fresco.initialize(this)
        } catch(e: Exception) {
            // Expected when running tests
            // see https://github.com/facebook/fresco/issues/2060
        }

        startKoin {
            androidLogger()
            androidContext(this@MobileApp)
            modules(AppModule.appModule)
        }
    }
}