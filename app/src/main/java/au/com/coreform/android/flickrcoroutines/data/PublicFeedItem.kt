package au.com.coreform.android.flickrcoroutines.data

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PublicFeedItem(val title: String,
                          val media: FlickrMedia,
                          val date_taken: String,
                          val description: String,
                          val author: String
)