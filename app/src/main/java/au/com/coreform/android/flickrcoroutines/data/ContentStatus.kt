package au.com.coreform.android.flickrcoroutines.data

import androidx.lifecycle.MutableLiveData

sealed class ContentStatus<T> {
    // The client has (generally) requested some content
    data class Seeking<T>(val generalRequest: () ->Pair<MutableLiveData<ContentStatus<T>>,
            MutableLiveData<T>>): ContentStatus<T>()
    // Seeking request being processed, attempting to retrieve content from local storage
    data class Retrieving<T>(val generalRequest: () -> Pair<MutableLiveData<ContentStatus<T>>,
            MutableLiveData<T>>): ContentStatus<T>()
    // Content found locally and is not stale
    data class SatisfiedLocally<T>(val generalRequest: () -> Pair<MutableLiveData<ContentStatus<T>>,
            MutableLiveData<T>>,
                                   val content: T): ContentStatus<T>()
    // Content found locally but is stale, so also attempting to fetch fresh data from Internet
    data class LocalStaleFetching<T>(val generalRequest: () -> Pair<MutableLiveData<ContentStatus<T>>,
            MutableLiveData<T>>,
                                     val staleContent: T): ContentStatus<T>()
    // No local content found (or is expired), so attempting to fetch fresh data from Internet
    data class NothingLocalFetching<T>(val generalRequest: () -> Pair<MutableLiveData<ContentStatus<T>>,
            MutableLiveData<T>>): ContentStatus<T>()
    // Failed to retrieve content from Internet
    data class FetchFailed<T>(val generalRequest: () -> Pair<MutableLiveData<ContentStatus<T>>,
            MutableLiveData<T>>,
                              val e: Exception): ContentStatus<T>()
    // Fresh content successfully pulled from Internet
    data class SatisfiedExternally<T>(val generalRequest: () -> Pair<MutableLiveData<ContentStatus<T>>,
            MutableLiveData<T>>,
                                      val content: T): ContentStatus<T>()
}