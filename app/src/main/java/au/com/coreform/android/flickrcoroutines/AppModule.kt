package au.com.coreform.android.flickrcoroutines

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.room.Room
import au.com.coreform.android.flickrcoroutines.comms.FlickrPublicFeedServiceAPI
import au.com.coreform.android.flickrcoroutines.display.viewmodel.MediaListViewModel
import au.com.coreform.android.flickrcoroutines.peristence.db.MobileDb
import au.com.coreform.android.flickrcoroutines.repositories.LocalMediaRepository
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object AppModule {
    @JvmStatic
    val appModule = module {
        single { FlickrPublicFeedServiceAPI() }
        single { get<FlickrPublicFeedServiceAPI>().getFlickrPublicFeedService() }
        single { Room.databaseBuilder(androidContext(),
            MobileDb::class.java,"MobileDb").build() }
        single { get<MobileDb>().flickrMediaDao() }
        single { androidContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }
        single { IntentFilter() }
        single { LocalMediaRepository(get(), get(), get()) }
        viewModel { MediaListViewModel(get(), get()) }
    }
}