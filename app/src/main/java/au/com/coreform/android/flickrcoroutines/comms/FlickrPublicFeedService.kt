package au.com.coreform.android.flickrcoroutines.comms

import au.com.coreform.android.flickrcoroutines.data.PublicFeedHeader
import au.com.coreform.android.flickrcoroutines.data.PublicFeedItem
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface FlickrPublicFeedService {
    @GET("services/feeds/photos_public.gne?format=json")
    suspend fun fetchContent(): Response<PublicFeedHeader>
}