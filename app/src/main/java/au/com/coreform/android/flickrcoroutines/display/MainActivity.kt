package au.com.coreform.android.flickrcoroutines.display

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import au.com.coreform.android.flickrcoroutines.R
import au.com.coreform.android.flickrcoroutines.data.ContentStatus
import au.com.coreform.android.flickrcoroutines.databinding.ActivityMainBinding
import au.com.coreform.android.flickrcoroutines.display.adapters.MediaListAdapter
import au.com.coreform.android.flickrcoroutines.display.viewmodel.MediaListViewModel
import au.com.coreform.android.flickrcoroutines.repositories.LocalMediaRepository
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    val model: MediaListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.setLifecycleOwner(this)
        binding.mediaRv.adapter = MediaListAdapter()
        binding.mediaRv.apply {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        val mediaRepo = get<LocalMediaRepository>()
        val mediaLoad = mediaRepo.ldLocalMedia()
        mediaLoad.first.observe(this, Observer { contentStatus ->
            when (contentStatus) {
                is ContentStatus.Seeking -> {
                    Log.d("MainActivity", "seeking...")
                    model.isSeeking = true
                    binding.isSeeking = true
                }
                is ContentStatus.Retrieving -> Log.d("MainActivity", "retrieving...")
                is ContentStatus.SatisfiedLocally -> {
                    Log.d("MainActivity", "satisfied locally.")
                    model.isSeeking = false
                    model.isInError = false
                    binding.isSeeking = false
                    binding.isInError = false
                    // avoid exIndexOutOfBoundsException: Inconsistency detected. Invalid view holder adapter position
                    // upon orientation change
                    binding.mediaRv.itemAnimator = null
                    binding.mediaRv.adapter?.let { adapter ->
                        Log.d("MainActivity", "Received local content: ${contentStatus.content.size}")
                        val mediaListAdapter = adapter as? MediaListAdapter
                        mediaListAdapter?.setItems(contentStatus.content)
                    }
                }
                is ContentStatus.LocalStaleFetching -> Log.d("MainActivity", "local stale, fetching...")
                is ContentStatus.NothingLocalFetching -> Log.d("MainActivity", "nothing local, fetching...")
                is ContentStatus.FetchFailed -> {
                    Log.d("MainActivity", "fetch failed...")
                    model.isSeeking = false
                    model.isInError = true
                    binding.isSeeking = false
                    binding.isInError = true
                }
                is ContentStatus.SatisfiedExternally -> {
                    Log.d("MainActivity", "satisfied externally.")
                    model.isSeeking = false
                    model.isInError = false
                    binding.isSeeking = false
                    binding.isInError = false
                    binding.mediaRv.adapter?.let { adapter ->
                        Log.d("MainActivity", "Received external content: ${contentStatus.content.size}")
                        val mediaListAdapter = adapter as? MediaListAdapter
                        mediaListAdapter?.setItems(contentStatus.content)
                    }
                }
            }
        })
    }
}