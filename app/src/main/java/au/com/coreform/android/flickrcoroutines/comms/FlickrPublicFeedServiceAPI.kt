package au.com.coreform.android.flickrcoroutines.comms

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

/*
 * Retrofit service for Flickr Public Feed endpoint.
 */
class FlickrPublicFeedServiceAPI() {
    fun getFlickrPublicFeedService() : FlickrPublicFeedService {
        val httpClient: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(JsonCleaningInterceptor())
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .build()
        val retrofit = Retrofit.Builder()
            .client(httpClient)
            .baseUrl("https://www.flickr.com")
            .addConverterFactory(MoshiConverterFactory.create().asLenient())
            .build()

        return retrofit.create(FlickrPublicFeedService::class.java)
    }
}