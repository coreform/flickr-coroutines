package au.com.coreform.android.flickrcoroutines.display.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import au.com.coreform.android.flickrcoroutines.R
import au.com.coreform.android.flickrcoroutines.data.LocalMediaItem
import au.com.coreform.android.flickrcoroutines.databinding.ItemCardMediaBinding

class MediaListAdapter: RecyclerView.Adapter<MediaListAdapter.ImageCardHolder>() {

    private var items: List<LocalMediaItem> = emptyList()
    private lateinit var binding: ItemCardMediaBinding

    fun setItems(newItems: List<LocalMediaItem>) {
        Log.d("MediaListAdapter", "Received new data...")
        items = newItems
        notifyItemRangeInserted(0, items.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageCardHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_card_media,
            parent,
            false
        )
        return ImageCardHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ImageCardHolder, position: Int) {
        holder.itemBinding.mediaItem = items[position]
        holder.itemBinding.executePendingBindings()
    }

    class ImageCardHolder(val itemBinding: ItemCardMediaBinding): RecyclerView.ViewHolder(itemBinding.root)
}

