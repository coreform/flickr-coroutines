package au.com.coreform.android.flickrcoroutines

import au.com.coreform.android.flickrcoroutines.comms.JsonCleaningInterceptor
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class JsonCleansiningTest {

    @Test
    fun `test something`() {
        val inputRawJson = """jsonFlickrFeed({
"title": "Uploads from everyone",
"link": "https:\/\/www.flickr.com\/photos\/",
"description": "",
"modified": "2019-08-06T03:18:33Z",
"generator": "https:\/\/www.flickr.com",
"items": []})"""
        val expectedOutputRawJson = """{
"title": "Uploads from everyone",
"link": "https:\/\/www.flickr.com\/photos\/",
"description": "",
"modified": "2019-08-06T03:18:33Z",
"generator": "https:\/\/www.flickr.com",
"items": []}"""
        val interceptor = JsonCleaningInterceptor()
        Assert.assertEquals(expectedOutputRawJson, interceptor.cleanseRawJson(inputRawJson))
    }
}