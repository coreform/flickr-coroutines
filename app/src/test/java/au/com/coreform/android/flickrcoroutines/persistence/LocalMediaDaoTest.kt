package au.com.coreform.android.flickrcoroutines.persistence

import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import au.com.coreform.android.flickrcoroutines.AppModule
import au.com.coreform.android.flickrcoroutines.data.LocalMediaItem
import au.com.coreform.android.flickrcoroutines.peristence.db.MobileDb
import au.com.coreform.android.flickrcoroutines.persistence.dao.LocalMediaDao
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.robolectric.annotation.Config
import java.time.ZonedDateTime

@Config(manifest=Config.NONE)
@RunWith(AndroidJUnit4::class)
class LocalMediaDaoTest: KoinTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @kotlinx.coroutines.ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI Thread")

    var appDb: MobileDb? = null
    var localMediaDao: LocalMediaDao? = null

    @Before
    @Throws(Exception::class)
    fun before() {
        // Robolectric will start koin with the Application
        Dispatchers.setMain(mainThreadSurrogate)
        appDb = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getInstrumentation().context,
            MobileDb::class.java)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();
        localMediaDao = appDb!!.flickrMediaDao()
    }

    @After
    fun after() {
        appDb!!.close()
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
        stopKoin()
    }

    @Test
    @Throws(InterruptedException::class)
    fun `get media from empty db`() {
        var mediaList: List<LocalMediaItem> = emptyList()
        runBlockingTest {
            mediaList = localMediaDao!!.listLocalMedia()
        }

        assertTrue(mediaList.isEmpty())
    }

    @Test
    @Throws(InterruptedException::class)
    fun `get media after inserted a media`() {
        val testMedia = LocalMediaItem()
        testMedia.title = "Test Media"
        testMedia.uri = Uri.EMPTY
        testMedia.taken_timestamp = System.currentTimeMillis()
        testMedia.taken_datetime = ZonedDateTime.now()
        var mediaList: List<LocalMediaItem> = emptyList()
        runBlockingTest {
            localMediaDao!!.insertMedia(testMedia)
            mediaList = localMediaDao!!.listLocalMedia()
        }

        assertTrue(mediaList.size == 1)
    }
}