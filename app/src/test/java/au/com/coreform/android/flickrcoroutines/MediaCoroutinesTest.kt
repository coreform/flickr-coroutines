package au.com.coreform.android.flickrcoroutines

import au.com.coreform.android.flickrcoroutines.repositories.LocalMediaRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import au.com.coreform.android.flickrcoroutines.data.LocalMediaItem
import junit.framework.Assert.assertTrue
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class MediaCoroutinesTest: KoinTest {
    val mediaRepo by inject<LocalMediaRepository>()
    @kotlinx.coroutines.ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI Thread")

    @Before
    @kotlinx.coroutines.ExperimentalCoroutinesApi
    fun before() {
        // Robolectric will start koin with the Application
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    @kotlinx.coroutines.ExperimentalCoroutinesApi
    fun after() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
        stopKoin()
    }

    @Test
    @kotlinx.coroutines.ExperimentalCoroutinesApi
    fun `no connectivity load media`() {
        var mediaList: List<LocalMediaItem> = emptyList()
        runBlockingTest {
            mediaList = mediaRepo.fetchFlickrPublicFeedMedia()
        }
        assertTrue(!mediaList.isEmpty())
    }
}