# Flickr API

Note: the Flickr API Terms of Service are 404ing. Thus I was not able to agree to the Flickr API Terms of Service. https://www.flickr.com/services/api/tos/

target: Flickr public feed.

target doco: https://www.flickr.com/services/feeds/docs/photos_public/


Note: top level of the response is a Javascript call (i.e. JSONP ready), may need cleansing before parsing.

Note: top level of the json itself is a JSON Object, which provides some meta data including feed localized feed title and a JSON Array containing feed items. Notable feed item fields are: title, media, description, and author. The media field is a JSON Object that provides a mapping of image sizes to image resources, e.g. "m"="https:\/\/image.url.here" (i.e. the image resource uri field in json is dynamic).

Note: date_taken field is an ISO8601 formatted date without any timezone information, assuming UTC.

Note: description value is wrapped in HTML paragraph tags and contains HTML hyperlink tags.

Note: there are 8 or so hard-coded localizations available, e.g. 'fr-fr', but no further granularity such as 'ch-fr'.

example GET request (includes quasi-localization):

https://www.flickr.com/services/feeds/photos_public.gne?format=json&lang=fr-fr

example response:

jsonFlickrFeed({
		"title": "Toutes les importations",
		"link": "https:\/\/www.flickr.com\/photos\/",
		"description": "",
		"modified": "2019-08-01T02:14:13Z",
		"generator": "https:\/\/www.flickr.com",
		"items": [
	   {
			"title": "04 Osprey with a fish",
			"link": "https:\/\/www.flickr.com\/photos\/annkelliott\/48427494396\/",
			"media": {"m":"https:\/\/live.staticflickr.com\/65535\/48427494396_5bc55cbafc_m.jpg"},
			"date_taken": "2019-07-30T20:00:22-08:00",
			"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/annkelliott\/\">annkelliott<\/a> a post\u00e9 une photo\u00a0:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/annkelliott\/48427494396\/\" title=\"04 Osprey with a fish\"><img src=\"https:\/\/live.staticflickr.com\/65535\/48427494396_5bc55cbafc_m.jpg\" width=\"240\" height=\"180\" alt=\"04 Osprey with a fish\" \/><\/a><\/p> ",
			"published": "2019-08-01T02:14:13Z",
			"author": "nobody@flickr.com (\"annkelliott\")",
			"author_id": "71833159@N00",
			"tags": ""
	   },
	   {
			"title": "\uc804\uad6d24\uc2dc#\ucd9c\uc7a5\uc548\ub9c8\ud6c4\uae30-070=7333=9649-[\uce74\ud1a1-VV23]\uc990\uac70\uc6b4\ub2ec\ub9bc#\ucd9c\uc7a5\uc548\ub9c8\ucda9\uc8fc[070-7333-9649]\ud589\ubcf5\ud55c\ub9cc\ub0a8\uc774\ubca4\ud2b8[\uce74\ud1a1VV23]#\ucd9c\uc7a5\uc548\ub9c8\ucd94\ucc9c#\ucd9c\uc7a5\uc548\ub9c8\uac15\ucd94[\uad6c\uae00\uac80\uc0c9-\ub291\ub300\uc640\uc5ec\ubc30\uc6b0]#\ucd9c\uc7a5\uc548\ub9c8\ud6c4\uae30",
			"link": "https:\/\/www.flickr.com\/photos\/182443472@N07\/48427494491\/",
			"media": {"m":"https:\/\/live.staticflickr.com\/65535\/48427494491_8cc11d6d75_m.jpg"},
			"date_taken": "2019-07-31T19:14:14-08:00",
			"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/182443472@N07\/\">igangseog463<\/a> a post\u00e9 une photo\u00a0:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/182443472@N07\/48427494491\/\" title=\"\uc804\uad6d24\uc2dc#\ucd9c\uc7a5\uc548\ub9c8\ud6c4\uae30-070=7333=9649-[\uce74\ud1a1-VV23]\uc990\uac70\uc6b4\ub2ec\ub9bc#\ucd9c\uc7a5\uc548\ub9c8\ucda9\uc8fc[070-7333-9649]\ud589\ubcf5\ud55c\ub9cc\ub0a8\uc774\ubca4\ud2b8[\uce74\ud1a1VV23]#\ucd9c\uc7a5\uc548\ub9c8\ucd94\ucc9c#\ucd9c\uc7a5\uc548\ub9c8\uac15\ucd94[\uad6c\uae00\uac80\uc0c9-\ub291\ub300\uc640\uc5ec\ubc30\uc6b0]#\ucd9c\uc7a5\uc548\ub9c8\ud6c4\uae30\"><img src=\"https:\/\/live.staticflickr.com\/65535\/48427494491_8cc11d6d75_m.jpg\" width=\"240\" height=\"240\" alt=\"\uc804\uad6d24\uc2dc#\ucd9c\uc7a5\uc548\ub9c8\ud6c4\uae30-070=7333=9649-[\uce74\ud1a1-VV23]\uc990\uac70\uc6b4\ub2ec\ub9bc#\ucd9c\uc7a5\uc548\ub9c8\ucda9\uc8fc[070-7333-9649]\ud589\ubcf5\ud55c\ub9cc\ub0a8\uc774\ubca4\ud2b8[\uce74\ud1a1VV23]#\ucd9c\uc7a5\uc548\ub9c8\ucd94\ucc9c#\ucd9c\uc7a5\uc548\ub9c8\uac15\ucd94[\uad6c\uae00\uac80\uc0c9-\ub291\ub300\uc640\uc5ec\ubc30\uc6b0]#\ucd9c\uc7a5\uc548\ub9c8\ud6c4\uae30\" \/><\/a><\/p> <p>#\ucd9c\uc7a5\ub9c8\uc0ac\uc9c0\ud6c4\uae30,#\ucd9c\uc7a5\ub9c8\uc0ac\uc9c0\ucd94\ucc9c\uc990\uac70\uc6b4\ub2ec\ub9bc#\ucd9c\uc7a5\uc548\ub9c8\ud30c\uc8fc[070-7333-9649]\ud589\ubcf5\ud55c\ub9cc\ub0a8\uc774\ubca4\ud2b8[\uce74\ud1a1VV23]#\ucd9c\uc7a5\uc548\ub9c8\ucd94\ucc9c#\ucd9c\uc7a5\uc548\ub9c8\uac15\ucd94[\uad6c\uae00\uac80\uc0c9-\ub291\ub300\uc640\uc5ec\ubc30\uc6b0]#\ucd9c\uc7a5\uc548\ub9c8\ud6c4\uae30<br \/> <br \/> <br \/> <a href=\"http:\/\/blackmon.dothome.co.kr\/%ec%a0%84%ea%b5%ad24%ec%8b%9c%ec%b6%9c%ec%9e%a5%ec%95%88%eb%a7%88%ed%9b%84%ea%b8%b0-07073339649-%ec%b9%b4%ed%86%a1-vv23%ec%a6%90%ea%b1%b0%ec%9a%b4%eb%8b%ac%eb%a6%bc%ec%b6%9c%ec%9e%a5%ec%95%88-294\/\" rel=\"noreferrer nofollow\">blackmon.dothome.co.kr\/%ec%a0%84%ea%b5%ad24%ec%8b%9c%ec%b...<\/a><\/p>",
			"published": "2019-08-01T02:14:14Z",
			"author": "nobody@flickr.com (\"igangseog463\")",
			"author_id": "182443472@N07",
			"tags": ""
	   }
})

